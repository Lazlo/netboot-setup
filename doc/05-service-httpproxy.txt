= Service HTTP proxy =

When you will do unattended installation of Debian or Ubuntu systems you
will repeatedly download packages from the internet via HTTP.

This can be speeded up dramatically by using a HTTP proxy that will
cache the packages loaded.

== squid-deb-proxy ==
We will use the squid proxy server to make this happen.
Install the "squid-deb-proxy" package. This will install a specially
configured squid server.

{{{apt-get install -y squid-deb-proxy}}}

Now you will need to configure who will be allowed to access the server and what to cache.

First edit the /etc/squid-deb-proxy/allowed-networks-src.acl and add a line like "192.168.1.0/24" to allow access for all hosts on the network.

Optionally add the domain names of the servers you want to be cached to
the /etc/squid-deb-proxy/mirror-dstdomain.acl.

{{{
security.debian.org
ftp2.de.debian.org
}}}

When done making changes to the configuration you need to make squid read the changed confguration. This can be done by running:

{{{
service squid-deb-proxy reload
}}}

By default the address of the proxy server will be http://<your-ip>:8000

Note: From time to time it might happen that a file has been loaded inconsistently (hashsum mismatch)
In such a case you can reconstruct the original URL from the squid-deb-proxy log files in /var/log/squid-deb-proxy.
When you found the URL and want to re-fetch, first open a new terminal where you can observe the squid-deb-proxy log.

tail /var/log/squid-deb-proxy

then re-fetch the file by running:

wget --no-cache <URL>

or using the squidclient command:

squidclient -s -h 192.168.122.1 -p 8000 -r http://de.archive.ubuntu.com/ubuntu/pool/main/e/eglibc/multiarch-support_2.15-0ubuntu10_i386.deb
