= Network Boot Installation Server =

== Hypervisor Network Setup ==
In order to have a virtual machine that will serve other virtual machines as boot/install server without disturbing the accual network the hypervisor machine is connected to (and relies on) we need to create a seperate isloated virtual network.

== New Virtual Machine ==
Create a new virtual machine that will be used as the boot/install server.
Make sure to configure the machine to have two network interfaces.
The first network interface will be used like on all other machines.
The second interface will be connected to the isolated virtual network.

=== Install OS ===
After having the hardware of the new virtual machine configured we can start installing the operating system (in our case Debian 6).
When installation is finished we need to configure the second network interface to have a static IP address on. This is the address the clients of the boot/install server will use for all services required.

=== DHCP ===
Now we will install a DHCP server (isc-dhcp-server) that will be used to netboot the clients.

After installing the software we need to configure the service in order to only provide its services on the second network interface.

This can be done by editing the /etc/default/isc-dhcp-server file and setting the following value:
{{{
INTERFACES="eth1"
}}}

Then we need to configure a DHCP range and information the server will push to its clients. Edit the /etc/dhcp/dhcpd.conf in the following way:
{{{
subnet 192.168.100.0 netmask 255.255.255.0 {

    range 192.168.100.10 192.168.100.20;

    option domain-name-servers 192.168.100.1;
    option routers 192.168.100.1;

    allow booting;
    allow bootp;

    filename "pxelinux.0";
    next-server 192.168.100.1;
}
}}}

=== Netboot Setup ===
Install {{{di-netboot-assistant}}}

Make sure /etc/di-netboot-assistant/di-netboot-assistant.conf is set to the
/var/lib/tftpboot directory.

=== Boot Menu ===
Create the following files in /srv/tftp

'''boot.txt'''
{{{
Description of possible boot options ...
}}}

'''pxelinux.cfg'''
{{{
DEFAULT squeeze_amd64_install

LABEL squeeze_amd64_install
    kernel debian/squeeze/amd64/linux
    append vga=normal initrd=debian/squeeze/amd64/initrd.gz

PROMPT 1
TIMEOUT 0
}}}

...

- create second new virtual machine
 - configure its network interface to be attached  to tbe virtual network
