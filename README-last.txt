netboot-setup

is the glue that makes a bunch of network services work together to provide
a network boot environment. the glue is actualy a collection of shell scripts
and configuration for the individual services involved in the setup.

using the scripts found here you can do things like:

 - manage boot images
 - manage boot loader menu
 - manage machines configuration
 - test configuration using virtual machines

there are (at least) N ways to use this setup. here are a some scenarios
 - all services on your workstation serving the local network
 - as above but with testing the netboot service inside virtual machines (using libvirt)
 - all services on a virtual machine that serves a virtual network
 - as about but with the vm serving the local network

dependencies

 - make

content of the project directory

 - doc/
 - scripts/
 - tests/
 - tftproot/

outline of services involved

 - DHCP
 - TFTP
 - NFS
 - HTTP
 - HTTP proxy
 - libvirt

the scripts found in this package automate the following tasks

 - setup-eth1 -- setup 2nd network interface
 - setup-nfs-share
 - install-images -- fetch operating system files and put them into NFS share
 - deploy-tftp-kernels
 - adjust-dnsmasq-conf -- adjusting dnsmasq.conf for working DHCP and TFTP
 - deploy-tftp-syslinux-menus -- create PXE config directory in tftp root and put boot menu files there
 - deploy-tftp-syslinux -- putting syslinux PXE files in the tftp root
 - FIXME -- setup httpd configuration
 - FIXME -- put preseed files into place
